<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Case</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>fcd22824-91a4-4ccd-b253-23d0e4354a00</testSuiteGuid>
   <testCaseLink>
      <guid>176b840b-054e-4a3b-aabb-b028aff8020b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62257dc5-8098-4cd7-9661-956373a2e3bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>626be443-15ad-4f13-b476-3ac0163b2540</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Logout Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c280ef01-fe80-46ec-be99-c71d668a9101</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail Less Than 8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee943486-7ff4-46df-bd9f-10a00e8e9803</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail Not Found Character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa381912-89db-46b1-ac95-b358b87216e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail Not Found Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a03668f0-a9d2-47ef-8ed6-987f5e98b96a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Fail Not Found Symbol</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3899aa2-1daf-4206-ae50-63195ba55cc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test Change Password Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
